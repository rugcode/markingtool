﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Drawing;

namespace Marking_Tool
{
	class Options
	{
		/// <summary>
		/// The location of the options file 
		/// </summary>
		private static string m_OptionsFileName = "&/Options.xml";

		/// <summary>
		/// The current state of the window (maximised or normal) 
		/// </summary>
		public FormWindowState WindowState { get; set; }

		/// <summary>
		/// The bounds of the window on the desktop
		/// </summary>
		public Rectangle Bounds { get; set; }

		/// <summary>
		/// The active marking helper xml file
		/// </summary>
		public string MarkingFilePath { get; set; }

		/// <summary>
		/// Wrap each comment in something (not implemented yet) 
		/// </summary>
		public bool WrapEachComment { get; set; }

		/// <summary>
		/// Load the options 
		/// </summary>
		public Options()
		{
			// set all options to their defaults 
			SetDefaults();

			// check to see if there is a options file to load 
			if (File.Exists(Helper.ResolvePath(m_OptionsFileName)) == false)
			{
				return;
			}

			// try to load the options
			Load();
		}

		private void SetDefaults()
		{
			// null the marking file
			MarkingFilePath = null;

			// set the bound to empty
			Bounds = Rectangle.Empty;

			// normal window state
			WindowState = FormWindowState.Normal;

			// whatever
			WrapEachComment = false;
		}

		private void Load()
		{
			try
			{
				XmlDocument doc = new XmlDocument();

				// load the options from the resolved path
				doc.Load(Helper.ResolvePath(m_OptionsFileName));

				XmlNode node = doc.DocumentElement;

				// if the node is not null 
				if (node != null)
				{
					// get the path of the marking file if one exists
					MarkingFilePath = Helper.GetAttributeValue(node, "MarkingFilePath", MarkingFilePath);

					// get rhe string for the bounding rectangle
					string boundsString = Helper.GetAttributeValue(node, "Bounds", Helper.SerializeRectangle(Bounds));

					try
					{
						// deserialize the bounding rectangle
						Bounds = Helper.DeserializeRectangle(boundsString);

						// if the bounds is not empty
						if (Bounds != Rectangle.Empty)
						{
							// check that the bounds is on the screen
							if (IsOnScreen(Bounds) == false)
							{
								// if the bounds is off the screen set it to empty 
								Bounds = Rectangle.Empty; 
							}
						}
					}
					catch (Exception ex)
					{
						// tell the user
						MessageBox.Show(ex.Message, "Could not parse window bounds");
					}

					// get the window state
					WindowState = Helper.GetAttributeValue(node, "WindowState", WindowState);

					// whatever
					WrapEachComment = Helper.GetAttributeValue(node, "WrapEachComment", WrapEachComment);
				}
			}
			catch (Exception ex)
			{
				// somthing went wrong, tell the user
				MessageBox.Show(ex.Message, "Could not load options");
			}
		}

		public void Save()
		{
			try
			{
				XmlDocument doc = new XmlDocument();

				XmlElement node = Helper.CreateElement(doc, "Options");

				Helper.AppendAttributeAndValue(node, "MarkingFilePath", MarkingFilePath);
				Helper.AppendAttributeAndValue(node, "Bounds", Helper.SerializeRectangle(Bounds));
				Helper.AppendAttributeAndValue(node, "WindowState", WindowState.ToString());
				Helper.AppendAttributeAndValue(node, "WrapEachComment", WrapEachComment);

				doc.AppendChild(node);

				Helper.EnsurePathExists(Helper.ResolvePath(m_OptionsFileName));
				doc.Save(Helper.ResolvePath(m_OptionsFileName));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Could not save options");
			}
		}

		/// <summary>
		/// Check that a rectangle is fully on the screen
		/// </summary>
		/// <param name="rectangle">the rectangle to check</param>
		/// <returns>true if the rectangle is fully on a screen</returns>
		private bool IsOnScreen(Rectangle rectangle)
		{
			Screen[] screens = Screen.AllScreens;
			foreach (Screen screen in screens)
			{
				if (screen.WorkingArea.Contains(rectangle))
				{
					return true;
				}
			}

			return false;
		}
	}
}
