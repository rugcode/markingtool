﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace Marking_Tool
{
	public partial class MainForm : Form
	{
		#region Private Members

		private const string m_BaseTitle = "Marking Helper";
		
		private Options m_Options = new Options(); 

		private bool m_ChangesHaveBeenMadeSinceLastSave = false;
		private string m_Template;
		private Dictionary<string, TreeNode> m_NodesLookup = new Dictionary<string, TreeNode>();				

		#endregion 

		public MainForm()
		{
			InitializeComponent();
		}


		#region Form Events

		private void MainForm_Load(object sender, EventArgs e)
		{
			SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
			SetStyle(ControlStyles.ResizeRedraw, false);

			if (m_Options.Bounds != Rectangle.Empty)
			{
				this.DesktopBounds = m_Options.Bounds;
			}

			WindowState = m_Options.WindowState;

			if (LoadXml(m_Options.MarkingFilePath) == false)
			{
				m_Options.MarkingFilePath = null; 
			}

			SetWindowTitle(); 
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (e.CloseReason == CloseReason.TaskManagerClosing) 
			{
				return; 
			}

			if (m_ChangesHaveBeenMadeSinceLastSave == true)
			{
				DialogResult result = MessageBox.Show("There are unsaved changes to the marking file. Do you want to save them?", "Save changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

				switch (result)
				{
					case DialogResult.Cancel:
						e.Cancel = true; 
						break;
					case DialogResult.No:
						break;
					case DialogResult.Yes:
						SaveFile();
						break;
					default:
						break;
				}				
			}	
		
			m_Options.Save(); 
		}

		#endregion


		#region File Handlers

		#region Set Window Title

		private void SetWindowTitle()
		{
			if (m_Options.MarkingFilePath == null)
			{
				this.Text = "Unsaved - " + m_BaseTitle;
			}
			else
			{
				this.Text = m_Options.MarkingFilePath + " - " + m_BaseTitle;
			}
		}

		#endregion

		#region New File

		private void NewFile()
		{
			m_Options.MarkingFilePath = null;
			ResetChangesFlag(); 

			m_CriteriaView.BeginUpdate();
			m_NodesLookup.Clear();
			m_CriteriaView.Nodes.Clear();
			m_CriteriaView.EndUpdate();

			SetWindowTitle(); 
		}

		#endregion

		#region Open

		private void OpenFile()
		{
			if (m_OpenFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				m_Options.MarkingFilePath = m_OpenFileDialog.FileName;

				if (LoadXml(m_Options.MarkingFilePath) == false)
				{
					m_Options.MarkingFilePath = null;
				}

				SetWindowTitle(); 
			}
		}

		#endregion

		#region Save File

		private void SaveFile()
		{
			if (m_Options.MarkingFilePath == null)
			{
				SaveFileAs();
			}
			else 
			{
				SaveXml(m_Options.MarkingFilePath); 
			}
		}

		#endregion 

		#region Save File As

		private void SaveFileAs()
		{				
			if (m_SaveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				m_Options.MarkingFilePath = m_SaveFileDialog.FileName;

				SaveXml(m_Options.MarkingFilePath);

				SetWindowTitle(); 
			}
		}

		#endregion 

		#region Exit

		private void Exit()
		{
			Close();
		}

		#endregion		

		#endregion


		#region Xml Handlers

		#region Load Xml

		private bool LoadXml(string xmlPath)
		{
			try
			{
				m_CriteriaView.BeginUpdate();
				m_NodesLookup.Clear();
				m_CriteriaView.Nodes.Clear();

				if (File.Exists(xmlPath) == false)
				{
					return false;
				}

				XmlDocument doc = new XmlDocument();

				doc.Load(xmlPath);

				XmlElement root = doc.DocumentElement;

				XmlNode templateNode = root.SelectSingleNode("Template");

				if (templateNode == null)
				{
					m_Template = "";
				}
				else
				{
					m_Template = templateNode.InnerText;
				}

				foreach (XmlNode sectionNode in root.SelectNodes("Section"))
				{
					if (sectionNode.Attributes["name"] == null)
					{
						continue;
					}

					TreeNode sectionTreeNode = m_CriteriaView.Nodes.Add(sectionNode.Attributes["name"].Value);

					sectionTreeNode.Tag = ParseContent(sectionNode);

					m_NodesLookup.Add(sectionTreeNode.FullPath, sectionTreeNode);

					foreach (XmlNode criteriaNode in sectionNode.SelectNodes("Criteria"))
					{
						if (criteriaNode.Attributes["name"] == null)
						{
							continue;
						}

						TreeNode criteriaTreeNode = sectionTreeNode.Nodes.Add(criteriaNode.Attributes["name"].Value);

						criteriaTreeNode.Tag = ParseContent(criteriaNode);

						m_NodesLookup.Add(criteriaTreeNode.FullPath, criteriaTreeNode);

						foreach (XmlNode commentNode in criteriaNode.SelectNodes("Comment"))
						{
							if (commentNode.Attributes["name"] == null)
							{
								continue;
							}

							TreeNode commentTreeNode = criteriaTreeNode.Nodes.Add(commentNode.Attributes["name"].Value);

							commentTreeNode.Tag = ParseContent(commentNode);

							m_NodesLookup.Add(commentTreeNode.FullPath, commentTreeNode);
						}
					}
				}
			}
			finally
			{
				m_CriteriaView.ExpandAll();
				m_CriteriaView.EndUpdate();
				ResetChangesFlag(); 
			}

			return true; 
		}

		private string ParseContent(XmlNode node)
		{
			XmlNode textNode = node.SelectSingleNode("Content");

			if (textNode != null)
			{
				return textNode.InnerText;
			}
			else
			{
				return ""; 
			}
		}

		#endregion

		#region Save Xml 

		private void SaveXml(string xmlPath)
		{
			XmlDocument doc = new XmlDocument();

			doc.AppendChild(doc.CreateElement("Marking"));

			XmlNode root = doc.DocumentElement;

			XmlElement templateNode = Helper.CreateElement(root, "Template");

			AppendContent(templateNode, m_Template);

			root.AppendChild(templateNode); 

			foreach (TreeNode sectionTreeNode in m_CriteriaView.Nodes)
			{
				XmlElement sectionNode = Helper.CreateElement(root, "Section");

				root.AppendChild(sectionNode);

				Helper.AppendAttributeAndValue(sectionNode, "name", sectionTreeNode.Text);

				if (sectionTreeNode.Tag != null && sectionTreeNode.Tag.ToString() != String.Empty)
				{
					AppendContent(sectionNode, sectionTreeNode.Tag.ToString()); 
				}

				foreach (TreeNode criteriaTreeNode in sectionTreeNode.Nodes)
				{
					XmlElement criteriaNode = Helper.CreateElement(sectionNode, "Criteria");

					sectionNode.AppendChild(criteriaNode);

					Helper.AppendAttributeAndValue(criteriaNode, "name", criteriaTreeNode.Text);

					if (criteriaTreeNode.Tag != null && criteriaTreeNode.Tag.ToString() != String.Empty)
					{
						AppendContent(criteriaNode, criteriaTreeNode.Tag.ToString());
					}

					foreach (TreeNode commentTreeNode in criteriaTreeNode.Nodes)
					{
						XmlElement commentNode = Helper.CreateElement(criteriaNode, "Comment");

						criteriaNode.AppendChild(commentNode);

						Helper.AppendAttributeAndValue(commentNode, "name", commentTreeNode.Text);

						if (commentTreeNode.Tag != null && commentTreeNode.Tag.ToString() != String.Empty)
						{
							AppendContent(commentNode, commentTreeNode.Tag.ToString());
						}
					}
				}
			}

			doc.Save(xmlPath); 
		}

		private void AppendContent(XmlNode node, string content)
		{
			XmlNode textNode = Helper.CreateElement(node, "Content"); 

			textNode.AppendChild(node.OwnerDocument.CreateCDataSection(content)); 

			node.AppendChild(textNode); 
		}

		#endregion 

		#endregion


		#region Show Messages Helpers

		private void ShowMessage_NoNodeExists(string path)
		{
			MessageBox.Show("No node exists at the path." + Environment.NewLine + path);
		}

		#endregion

		#region Nodes

		#region Node Path

		#region Path Helpers

		private string CurrentPath()
		{
			return m_PathBox.Text.Trim();
		}

		private bool DoesCurrentPathExist()
		{
			return DoesPathExist(CurrentPath());
		}

		private bool DoesPathExist(string path)
		{
			return m_NodesLookup.ContainsKey(path);
		}

		private void UpdatePathBoxColor()
		{
			bool exists = DoesCurrentPathExist();

			if (exists == true)
			{
				m_PathBox.BackColor = Color.LightGreen;
				m_UpdateNodeButton.Text = "Update";
			}
			else
			{
				m_PathBox.BackColor = Color.LightPink;
				m_UpdateNodeButton.Text = "Add";
			}
		}

		#endregion

		#region Path Changed

		private void PathBox_TextChanged(object sender, EventArgs e)
		{
			UpdatePathBoxColor();
		}

		#endregion

		#endregion

		#region Changes Have Been Made

		private void ChangesHaveBeenMade()
		{
			m_ChangesHaveBeenMadeSinceLastSave = true; 
		}

		private void ResetChangesFlag()
		{
			m_ChangesHaveBeenMadeSinceLastSave = false; 
		}

		#endregion 

		#region Add / Update Node

		private void UpdateNodeButton_Click(object sender, EventArgs e)
		{
			try
			{
				m_CriteriaView.BeginUpdate();

				string path = m_PathBox.Text.Trim();
				bool exists = DoesCurrentPathExist();

				if (exists == true)
				{
					m_NodesLookup[path].Tag = m_CommentBox.Text;
				}
				else
				{
					int index = path.LastIndexOf(m_CriteriaView.PathSeparator);

					if (index > 0)
					{
						string parentPath = path.Substring(0, index);
						string nodeName = path.Substring(index + 1);

						if (DoesPathExist(parentPath) == false)
						{
							ShowMessage_NoNodeExists(parentPath);

							return;
						}						

						TreeNode parent = m_NodesLookup[parentPath];

						TreeNode newNode = parent.Nodes.Add(nodeName);

						newNode.Tag = m_CommentBox.Text;

						m_NodesLookup.Add(newNode.FullPath, newNode);					
					}
					else
					{
						TreeNode newNode = m_CriteriaView.Nodes.Add(path);

						newNode.Tag = m_CommentBox.Text;

						m_NodesLookup.Add(newNode.FullPath, newNode);
					}
				}

				ChangesHaveBeenMade();			
			}
			finally
			{
				UpdatePathBoxColor();

				m_CriteriaView.ExpandAll();

				m_CriteriaView.EndUpdate();
			}
		}

		#endregion

		#region Delete Node

		private void DeleteNodeButton_Click(object sender, EventArgs e)
		{
			try 
			{
				m_CriteriaView.BeginUpdate();

				bool exists = DoesCurrentPathExist();

				if (exists == false)
				{
					ShowMessage_NoNodeExists(CurrentPath());
					return; 
				}

			 	DialogResult result = MessageBox.Show("Sure you want to delete the node at the path." + Environment.NewLine + CurrentPath(), "Delete Node", MessageBoxButtons.OKCancel);

				if (result == System.Windows.Forms.DialogResult.Cancel)
				{
					return; 
				}
				
				List<string> toRemove = new List<string>();

				toRemove.Add(CurrentPath()); 

				string subPath = CurrentPath() + m_CriteriaView.PathSeparator;

				foreach (string key in m_NodesLookup.Keys)
				{
					if (key.StartsWith(subPath) == true)
					{
						toRemove.Add(key); 
					}
				}

				toRemove.Reverse(); 

				foreach (string path in toRemove)
				{
					m_NodesLookup[path].Remove();
				
					m_NodesLookup.Remove(path); 
				}

				ChangesHaveBeenMade();			
			}
			finally
			{
				UpdatePathBoxColor();

				m_CriteriaView.ExpandAll();

				m_CriteriaView.EndUpdate();
			}
		}

		#endregion

		#endregion 

		#region Other Controls Events

		private void ClearButton_Click(object sender, EventArgs e)
		{
			m_FeedbackBox.Clear();
			m_FeedbackBox.AppendText(m_Template); 
		}

		private void CopyButton_Click(object sender, EventArgs e)
		{
			m_FeedbackBox.SelectAll();
			m_FeedbackBox.Copy(); 
		}

		private void RemoveButton_Click(object sender, EventArgs e)
		{
			m_FeedbackBox.Text = m_FeedbackBox.Text.Replace(m_CommentBox.Text, ""); 
		}

		private void AddButton_Click(object sender, EventArgs e)
		{
			m_FeedbackBox.AppendText(m_CommentBox.Text); 
		}

		private void CriteriaView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			m_CriteriaView.BeginUpdate();

			m_CommentBox.ClearUndo(); 
			m_CommentBox.Clear();
			m_CommentBox.AppendText(e.Node.Tag.ToString());

			m_CriteriaView.ExpandAll();

			m_CriteriaView.EndUpdate();

			m_PathBox.Text = e.Node.FullPath;
		}

		private void CriteriaView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			CriteriaView_NodeMouseClick(sender, e);

			AddButton_Click(sender, EventArgs.Empty); 
		}

		private void CriteriaView_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
		{
			e.Cancel = true;
		}

		#endregion

		#region Menu Events

		private void newToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NewFile(); 
		}

		private void openToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OpenFile(); 
		}

		private void saveToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveFile(); 
		}

		private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveFileAs(); 
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Exit(); 
		}

		#endregion 

		#region Window Resize / Move Events

		private void MainForm_ResizeBegin(object sender, EventArgs e)
		{		

		}

		private void MainForm_ResizeEnd(object sender, EventArgs e)
		{
			if (WindowState == FormWindowState.Normal)
			{
				m_Options.Bounds = this.DesktopBounds;
			}
		}

		private void MainForm_Resize(object sender, EventArgs e)
		{
			if (WindowState != FormWindowState.Minimized)
			{
				m_Options.WindowState = WindowState;
			}
		}

		#endregion
	}
}
