﻿namespace Marking_Tool
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_FeedbackBox = new System.Windows.Forms.RichTextBox();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.m_CriteriaView = new System.Windows.Forms.TreeView();
			this.m_CommentBox = new System.Windows.Forms.RichTextBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.m_DeleteNodeButton = new System.Windows.Forms.Button();
			this.m_UpdateNodeButton = new System.Windows.Forms.Button();
			this.m_PathBox = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.m_RemoveButton = new System.Windows.Forms.Button();
			this.m_CopyButton = new System.Windows.Forms.Button();
			this.m_AddButton = new System.Windows.Forms.Button();
			this.m_ClearButton = new System.Windows.Forms.Button();
			this.m_MenuStrip = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.m_SaveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.m_OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.m_MenuStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// m_FeedbackBox
			// 
			this.m_FeedbackBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_FeedbackBox.Location = new System.Drawing.Point(0, 0);
			this.m_FeedbackBox.Name = "m_FeedbackBox";
			this.m_FeedbackBox.Size = new System.Drawing.Size(768, 170);
			this.m_FeedbackBox.TabIndex = 0;
			this.m_FeedbackBox.Text = "";
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.splitContainer1.Location = new System.Drawing.Point(0, 24);
			this.splitContainer1.Margin = new System.Windows.Forms.Padding(1);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.m_FeedbackBox);
			this.splitContainer1.Size = new System.Drawing.Size(768, 515);
			this.splitContainer1.SplitterDistance = 341;
			this.splitContainer1.TabIndex = 1;
			// 
			// splitContainer2
			// 
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.m_CriteriaView);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.m_CommentBox);
			this.splitContainer2.Panel2.Controls.Add(this.panel2);
			this.splitContainer2.Panel2.Controls.Add(this.panel1);
			this.splitContainer2.Size = new System.Drawing.Size(768, 341);
			this.splitContainer2.SplitterDistance = 318;
			this.splitContainer2.TabIndex = 0;
			// 
			// m_CriteriaView
			// 
			this.m_CriteriaView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_CriteriaView.FullRowSelect = true;
			this.m_CriteriaView.Location = new System.Drawing.Point(0, 0);
			this.m_CriteriaView.Name = "m_CriteriaView";
			this.m_CriteriaView.ShowPlusMinus = false;
			this.m_CriteriaView.ShowRootLines = false;
			this.m_CriteriaView.Size = new System.Drawing.Size(318, 341);
			this.m_CriteriaView.TabIndex = 0;
			this.m_CriteriaView.BeforeCollapse += new System.Windows.Forms.TreeViewCancelEventHandler(this.CriteriaView_BeforeCollapse);
			this.m_CriteriaView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.CriteriaView_NodeMouseClick);
			this.m_CriteriaView.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.CriteriaView_NodeMouseDoubleClick);
			// 
			// m_CommentBox
			// 
			this.m_CommentBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_CommentBox.Location = new System.Drawing.Point(0, 31);
			this.m_CommentBox.Name = "m_CommentBox";
			this.m_CommentBox.Size = new System.Drawing.Size(446, 281);
			this.m_CommentBox.TabIndex = 2;
			this.m_CommentBox.Text = "";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.m_DeleteNodeButton);
			this.panel2.Controls.Add(this.m_UpdateNodeButton);
			this.panel2.Controls.Add(this.m_PathBox);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(446, 31);
			this.panel2.TabIndex = 3;
			// 
			// m_DeleteNodeButton
			// 
			this.m_DeleteNodeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.m_DeleteNodeButton.Location = new System.Drawing.Point(301, 4);
			this.m_DeleteNodeButton.Name = "m_DeleteNodeButton";
			this.m_DeleteNodeButton.Size = new System.Drawing.Size(73, 23);
			this.m_DeleteNodeButton.TabIndex = 3;
			this.m_DeleteNodeButton.Text = "Delete";
			this.m_DeleteNodeButton.UseVisualStyleBackColor = true;
			this.m_DeleteNodeButton.Click += new System.EventHandler(this.DeleteNodeButton_Click);
			// 
			// m_UpdateNodeButton
			// 
			this.m_UpdateNodeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.m_UpdateNodeButton.Location = new System.Drawing.Point(380, 4);
			this.m_UpdateNodeButton.Name = "m_UpdateNodeButton";
			this.m_UpdateNodeButton.Size = new System.Drawing.Size(63, 23);
			this.m_UpdateNodeButton.TabIndex = 2;
			this.m_UpdateNodeButton.Text = "Update";
			this.m_UpdateNodeButton.UseVisualStyleBackColor = true;
			this.m_UpdateNodeButton.Click += new System.EventHandler(this.UpdateNodeButton_Click);
			// 
			// m_PathBox
			// 
			this.m_PathBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_PathBox.Location = new System.Drawing.Point(3, 6);
			this.m_PathBox.Name = "m_PathBox";
			this.m_PathBox.Size = new System.Drawing.Size(292, 20);
			this.m_PathBox.TabIndex = 0;
			this.m_PathBox.TextChanged += new System.EventHandler(this.PathBox_TextChanged);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.m_RemoveButton);
			this.panel1.Controls.Add(this.m_CopyButton);
			this.panel1.Controls.Add(this.m_AddButton);
			this.panel1.Controls.Add(this.m_ClearButton);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 312);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(446, 29);
			this.panel1.TabIndex = 1;
			// 
			// m_RemoveButton
			// 
			this.m_RemoveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.m_RemoveButton.Location = new System.Drawing.Point(301, 3);
			this.m_RemoveButton.Name = "m_RemoveButton";
			this.m_RemoveButton.Size = new System.Drawing.Size(73, 23);
			this.m_RemoveButton.TabIndex = 3;
			this.m_RemoveButton.Text = "Remove";
			this.m_RemoveButton.UseVisualStyleBackColor = true;
			this.m_RemoveButton.Click += new System.EventHandler(this.RemoveButton_Click);
			// 
			// m_CopyButton
			// 
			this.m_CopyButton.Location = new System.Drawing.Point(84, 3);
			this.m_CopyButton.Name = "m_CopyButton";
			this.m_CopyButton.Size = new System.Drawing.Size(75, 23);
			this.m_CopyButton.TabIndex = 2;
			this.m_CopyButton.Text = "Copy All";
			this.m_CopyButton.UseVisualStyleBackColor = true;
			this.m_CopyButton.Click += new System.EventHandler(this.CopyButton_Click);
			// 
			// m_AddButton
			// 
			this.m_AddButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.m_AddButton.Location = new System.Drawing.Point(380, 3);
			this.m_AddButton.Name = "m_AddButton";
			this.m_AddButton.Size = new System.Drawing.Size(63, 23);
			this.m_AddButton.TabIndex = 1;
			this.m_AddButton.Text = "Add";
			this.m_AddButton.UseVisualStyleBackColor = true;
			this.m_AddButton.Click += new System.EventHandler(this.AddButton_Click);
			// 
			// m_ClearButton
			// 
			this.m_ClearButton.Location = new System.Drawing.Point(3, 3);
			this.m_ClearButton.Name = "m_ClearButton";
			this.m_ClearButton.Size = new System.Drawing.Size(75, 23);
			this.m_ClearButton.TabIndex = 0;
			this.m_ClearButton.Text = "Clear All";
			this.m_ClearButton.UseVisualStyleBackColor = true;
			this.m_ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
			// 
			// m_MenuStrip
			// 
			this.m_MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
			this.m_MenuStrip.Location = new System.Drawing.Point(0, 0);
			this.m_MenuStrip.Name = "m_MenuStrip";
			this.m_MenuStrip.Size = new System.Drawing.Size(768, 24);
			this.m_MenuStrip.TabIndex = 2;
			this.m_MenuStrip.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator2,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// newToolStripMenuItem
			// 
			this.newToolStripMenuItem.Name = "newToolStripMenuItem";
			this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
			this.newToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
			this.newToolStripMenuItem.Text = "&New";
			this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.openToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
			this.openToolStripMenuItem.Text = "&Open";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(181, 6);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
			this.saveToolStripMenuItem.Text = "&Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
			this.saveAsToolStripMenuItem.Text = "&Save as";
			this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(181, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
			this.exitToolStripMenuItem.Text = "&Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// m_SaveFileDialog
			// 
			this.m_SaveFileDialog.Filter = "Xml files|*.xml|All files|*.*";
			this.m_SaveFileDialog.Title = "Save Marking Template";
			// 
			// m_OpenFileDialog
			// 
			this.m_OpenFileDialog.Filter = "Xml files|*.xml|All files|*.*";
			this.m_OpenFileDialog.Title = "Open Marking Template";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(768, 539);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.m_MenuStrip);
			this.MainMenuStrip = this.m_MenuStrip;
			this.MinimumSize = new System.Drawing.Size(506, 518);
			this.Name = "MainForm";
			this.Text = "Marking Helper";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.ResizeBegin += new System.EventHandler(this.MainForm_ResizeBegin);
			this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
			this.Resize += new System.EventHandler(this.MainForm_Resize);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.m_MenuStrip.ResumeLayout(false);
			this.m_MenuStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.RichTextBox m_FeedbackBox;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.TreeView m_CriteriaView;
		private System.Windows.Forms.RichTextBox m_CommentBox;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button m_RemoveButton;
		private System.Windows.Forms.Button m_CopyButton;
		private System.Windows.Forms.Button m_AddButton;
		private System.Windows.Forms.Button m_ClearButton;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button m_DeleteNodeButton;
		private System.Windows.Forms.Button m_UpdateNodeButton;
		private System.Windows.Forms.TextBox m_PathBox;
		private System.Windows.Forms.MenuStrip m_MenuStrip;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.SaveFileDialog m_SaveFileDialog;
		private System.Windows.Forms.OpenFileDialog m_OpenFileDialog;
	}
}

